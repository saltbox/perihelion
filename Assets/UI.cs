using UnityEngine;
using System.Collections;

public class UI : MonoBehaviour {
    GameObject[] obj;
    Vector3 screenPos;
    Camera cam;
	// Use this for initialization
	void Start () {
        obj = GameObject.FindGameObjectsWithTag("Player") as GameObject[];
        cam = Camera.main;
	}
	
	// OnGui is called once per frame
    void OnGUI()
    {
        
        foreach (GameObject o in obj)
        {
            screenPos = cam.WorldToScreenPoint(o.transform.position);
            GUI.Label(new Rect(screenPos.x, screenPos.y, 100, 50), "sup");
        }
        //GUI.Label(new Rect(0, 0, 50, 40), "sup");
    }
}
