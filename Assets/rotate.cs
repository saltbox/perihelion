using UnityEngine;
using System.Collections;

public class rotate : MonoBehaviour {

	// Use this for initialization
    Vector3 velocity;
	void Start () {
        velocity = new Vector3(-1, 0, 0);
		if (Input.GetKey(KeyCode.A))
		{
			velocity.x += 0.1f;	
			velocity.y -= 0.1f;
		}
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(velocity);
	}
}
