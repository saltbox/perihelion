using UnityEngine;
using System.Collections;

public class SmoothFollowControls : MonoBehaviour {
	
	// The target we are following
	public Transform target;
	// The distance in the x-z plane to the target
	float distance = 10.0f;
	// the height we want the camera to be above the target
	public float height = 500.0f;
	// How much we 
	float heightDamping = 5.0f;
	float rotationDamping = 3.0f;
	Vector3 position;

	
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate () {
		// Early out if we don't have a target
		if (!target)
			return;

		
		
		if (height >= 10)
		{
			height -= Input.GetAxis("Zoom") * height / 2;
			if (Input.GetKey(KeyCode.Minus))
				height += height / 20;
			if (Input.GetKey(KeyCode.Equals))
				height -= height / 20;	
		}
		else
		{
			height = 10;
		}
					
		
		// Calculate the current rotation angles
		float wantedRotationAngle = 0;
		float wantedHeight = target.position.y + height;
			
		float currentRotationAngle = transform.eulerAngles.y;
		float currentHeight = transform.position.y;
		
		// Damp the rotation around the y-axis
		currentRotationAngle = Mathf.LerpAngle (currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);
		// Damp the height
		currentHeight = Mathf.Lerp (currentHeight, wantedHeight, heightDamping * Time.deltaTime);
	
		// Convert the angle into a rotation
		Quaternion currentRotation = Quaternion.Euler (0, currentRotationAngle, 0);
		
		// Set the position of the camera on the x-z plane to:
		// distance meters behind the target
		position = target.position;
		position -= currentRotation * Vector3.forward * distance;
		position.y = currentHeight;
		
		transform.position = position;
		
		// Always look at the target
		transform.LookAt (target);
	}
	
	public void SetTarget(Transform t)
	{
		target = t;
	}
}
