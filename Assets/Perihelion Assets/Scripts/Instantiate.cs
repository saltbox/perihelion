using UnityEngine;
using System.Collections;

public class Instantiate : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	GameObject spaceCraft;
	
	void OnNetworkLoadedLevel () {	
		// Instantiating SpaceCraft when Network is loaded		
		Network.Instantiate(spaceCraft, transform.position, transform.rotation, 0);
		spaceCraft.name = GameObject.Find("Main Camera").GetComponent<MainGUI_Manager>().UserName;
	}
	
	void OnPlayerDisconnected (NetworkPlayer player) {
		Network.RemoveRPCs(player, 0);
		Network.DestroyPlayerObjects(player);
	}
}
