using UnityEngine;
using System.Collections;

public class Cannon : Weapon {
	float damage;
	float reload;

	// Use this for initialization
	void Start () {
		player = transform.parent.gameObject.GetComponent<GunMount>().player;
		damage = 10;
	}
	
	// Update is called once per frame
	void Update () {
		reload -= Time.deltaTime;
	}
	
	public override void Fire()
	{
		if (reload <= 0)
		{	
			GameObject bullet = (GameObject) Network.Instantiate(player.Bulletfab, transform.position + (40 * transform.forward), transform.rotation,0);
			bullet.GetComponent<Bullet>().initialize(player.Velocity + (transform.forward * 2));	
			Debug.Log("Cannon Firing");
			reload = 5;
		}
	}
}
