using UnityEngine;
using System.Collections;

public class Bullet : GravItem
{
	
	public int SendRate = 60;
	
	// Use this for initialization
	public override void Start () {
		base.Start();
	}	
	// Update is called once per frame
	int sendCount = 0;
	int tickCount = 0;
	public override void Update () {
		base.Update();
				
		transform.position += Velocity;
		
		if(Network.isServer && sendCount == SendRate)
		{
			networkView.RPC("NetworkUpdate",RPCMode.Others,this.transform.position, Velocity, transform.rotation);
			sendCount = 0;
		}
		
		if(tickCount == 10000 && Network.isServer)
		{
			networkView.RPC("RPC_Destroy",RPCMode.All);
		}
		
		tickCount ++;
		sendCount ++;
	}
	
	void OnCollisionEnter(Collision collision) 
	{
		if(Network.isServer)
		{
			networkView.RPC("RPC_Destroy",RPCMode.All);
		}
	}
	
	[RPC]
	void RPC_Destroy()
	{
		Destroy(this.gameObject);
		//Play Explostion Animation
	}
	
	[RPC]
	void NetworkUpdate(Vector3 Pos, Vector3 Vel, Quaternion Rot)
	{
		this.transform.position = Pos;
		this.Velocity = Vel;
		this.transform.rotation = Rot;
	}
	
	public void initialize(Vector3 Vel)
	{
		networkView.RPC("NetworkUpdate",RPCMode.All , transform.position, Vel, transform.rotation);
	}
}
