using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {
	public Player player;
	
	// Use this for initialization
	void Start () {
		player = transform.parent.gameObject.GetComponent<GunMount>().player;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public virtual void Fire()
	{
		
	}
}
