using UnityEngine;
using System.Collections;

public class Missile : GravItem {
	
	public GameObject Target;
	public float AccelSpeed = 0.00001f;
	public int SendRate = 60;
	public float RotateSpeed = 12f;
	public float lifeTime;
	TrailRenderer trail;
	
	
	public override void Start ()		
	{
		base.Start ();
		lifeTime = 0;
		trail = gameObject.GetComponent<TrailRenderer>();
		trail.time = 0;
	}
	
	public float dirNum;
	
	int sendCount = 0;
	int tickCount = 0;
	public override void Update ()
	{
		base.Update ();
		
		lifeTime += Time.deltaTime;
		
		if (lifeTime > 2)
		{
			Velocity += (transform.forward * AccelSpeed)  * Time.deltaTime * rigidbody.mass;	
			
			trail.time = 6;	
		
		
			if(Target != null)
			{
				Vector3 heading = Target.transform.position - transform.position;
						
				dirNum = AngleDir(transform.forward, heading, transform.up);
				
				if(dirNum == 1)
				{
					Velocity += (transform.right * (AccelSpeed)) * Time.deltaTime* rigidbody.mass;
				}
				else if(dirNum == -1)
				{
					Velocity -= (transform.right * (AccelSpeed)) * Time.deltaTime* rigidbody.mass;
				}
			}
		}
		
		if (Target != null)
		{
			
			//Rotate To Face Target
			Quaternion targetRotation = Quaternion.LookRotation(Target.transform.position - transform.position);
	       	// Smoothly rotate towards the target point.
	        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, RotateSpeed * Time.time);
			
		}	
		
		transform.position += Velocity;
		transform.position = new Vector3(transform.position.x,0,transform.position.z);
		
		if(Network.isServer && sendCount == SendRate)
		{
			networkView.RPC("NetworkUpdate",RPCMode.Others,this.transform.position, Velocity, transform.rotation);
			sendCount = 0;
		}
		
		if(tickCount == 10000 && Network.isServer)
		{
			networkView.RPC("RPC_Destroy",RPCMode.All);
		}
		
		tickCount ++;
		sendCount ++;
	}
	
	void OnCollisionEnter(Collision collision) 
	{
		if(Network.isServer && collision.gameObject.name != "Missile")
		{
			networkView.RPC("RPC_Destroy",RPCMode.All);
		}
	}
	
	[RPC]
	void RPC_Destroy()
	{
		Destroy(this.gameObject);
		//Play Explostion Animation
	}
	
	[RPC]
	void NetworkUpdate(Vector3 Pos, Vector3 Vel, Quaternion Rot)
	{
		this.transform.position = Pos;
		this.Velocity = Vel;
		this.transform.rotation = Rot;
		
	}
	
	[RPC]
	void SetTarget(NetworkViewID TargetID)
	{
		this.Target = NetworkView.Find(TargetID).gameObject;
	}
	
	public void initialize(Vector3 Vel, GameObject targ)
	{
		networkView.RPC("NetworkUpdate",RPCMode.All , transform.position, Vel, transform.rotation);
		networkView.RPC("SetTarget",RPCMode.All , targ.networkView.viewID);
	}
	
	float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up) {
		Vector3 perp = Vector3.Cross(fwd, targetDir);
		float dir = Vector3.Dot(perp, up);
		if (dir > 0f) {	
			return 1f;
		} else if (dir < 0f) {
			return -1f;
		} else {
			return 0f;
		}
	}	
}
