using UnityEngine;
using System.Collections;

public class Laser : Weapon {
	float damage;
	
	// Use this for initialization
	void Start () {
		damage = 10;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public override void Fire()
	{
		RaycastHit info;
		
		if (Physics.Raycast(transform.position, transform.forward, out info))
		{
			GameObject hit = info.transform.gameObject;
			Debug.DrawLine(transform.position, info.point);
			if (hit.tag == "Player")
				hit.GetComponent<Player>().Damage(damage);
		}
	}	
}
