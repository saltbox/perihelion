using UnityEngine;
using System.Collections;

public class DustOrbit : MonoBehaviour {
	
	public Transform ParentBody;
	public Vector3 Velocity;
	public float Distance;
	public int SendRate = 120;
	public string teamName;
	public Color teamColor;
	public Color currentColor;
	
	bool FirstTick = true;
	
	// Use this for initialization
	void Start () {
		currentColor = renderer.material.color;
		teamColor = Color.white;
	}
	
	[RPC]
	void NetworkUpdate(Vector3 Pos, Vector3 Vel)
	{
		this.transform.position = Pos;
		this.Velocity = Vel;
	}
	
	// Update is called once per frame
	int tickCount = 0;
	void Update () {
				
		Distance = Vector3.Distance(this.transform.position, ParentBody.position);
		
		if(FirstTick)
		{
			FirstTick = false;
		}
		else
		{		
			Velocity += (0.1f * Vector3.Normalize(ParentBody.position - this.transform.position) *
                (this.transform.rigidbody.mass * ParentBody.rigidbody.mass) /
				(Vector3.Distance(this.transform.position, ParentBody.position)* 
				Vector3.Distance(this.transform.position, ParentBody.position)) / (this.transform.rigidbody.mass));
			
			if(ParentBody.tag != "Star")
			{
				//Use Parents Data
				DustOrbit Comp = ParentBody.GetComponent<DustOrbit>();
				
				transform.position += Comp.Velocity;
				teamName = Comp.teamName;
				teamColor = Comp.teamColor;			
			}
			
			if(teamColor != currentColor)
			{
				currentColor = teamColor;
				renderer.material.color = currentColor;
			}		
		}
				
		transform.position += Velocity;
		
		if(Network.isServer && tickCount == SendRate)
		{
			networkView.RPC("NetworkUpdate",RPCMode.Others,this.transform.position,Velocity);
			tickCount = 0;
		}
		
		tickCount ++;
	}
}
