using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Team
{
	//this is a pretty self-explanatory class
	public string name { get; private set; }
	public Color color { get; private set; }
	Dictionary<string,bool> players;
		
	public Team(string name)
	{
		this.name = name;
		color = Color.green;
		players = new Dictionary<string, bool>();
	}
	
	public void AddPlayer(string name, bool ready)
	{
		players.Add(name, ready);
	}
	
	public void RemovePlayer(string name)
	{
		players.Remove(name);
	}
	
	public void SetName(string s)
	{
		name = s;
	}
	
	public void SetColour(Color c)
	{
		color = c;
	}
	
	public Dictionary<string,bool> GetPlayers()
	{
		return players;
	}
	
}
