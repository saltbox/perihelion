using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainGUI_Manager : MonoBehaviour {
	
	List<GUI_State> RemoveStates;
	List<GUI_State> CurrentStates;
	List<GUI_State> AddStates;
	
	//States
	public Lobby lobby;
	MainMenu main;
	NameMenu nameMenu;
	CelestialBodyLabels labels;
	IngameGUI inGame;
	SpawnMenu spawn;
	CustomMenu custom;
	EscMenu esc;
	
	//Username
	public string UserName;
	
	public Dictionary<string, GUI_State> StateNames;
	
	// Use this for initialization
	void Start () 
	{
		//initialize State Storage
		StateNames = new Dictionary<string, GUI_State>();	
		//initialize State Lists
		CurrentStates = new List<GUI_State>();
		AddStates = new List<GUI_State>();
		RemoveStates = new List<GUI_State>();
		
		
		initialize();
	}
	
	void initialize()
	{
		//initialize states here
		main = new MainMenu("MainMenu", this, networkView);
		lobby = new Lobby("Lobby", this, networkView);
		nameMenu = new NameMenu("NameMenu", this, networkView);
		labels = new CelestialBodyLabels("Labels", this, networkView);
		inGame = new IngameGUI("inGame", this, networkView);
		spawn = new SpawnMenu("spawn", this, networkView);
		custom = new CustomMenu("custom", this, networkView);
		esc = new EscMenu("esc", this, networkView);
		
		//Push First Menu
		Push("NameMenu");
		Push("Labels");
	}
		
	void Update () 
	{	
		//Update State Lists
		foreach(GUI_State state in AddStates)
		{
			CurrentStates.Add(state);
		}
		AddStates.Clear();
		foreach(GUI_State state in CurrentStates)
		{
			state.Update();
		}
		foreach(GUI_State state in RemoveStates)
		{
			CurrentStates.Remove(state);
		}
		RemoveStates.Clear();
		
		//Update Other Needed Things
		UserName = nameMenu.CurrentUser;
		
	}
	void OnGUI()
	{
		foreach(GUI_State state in CurrentStates)
		{
			state.OnGUI();
		}
	}
	
	public GUI_State getState(string Name)
	{
		GUI_State value;
		if(StateNames.TryGetValue(Name,out value))
		{
			return(value);
		}
		else
		{
			throw new UnityException("State Not Found");
		}
	}	
	public void Push(string Name)
	{
		GUI_State value;
		if(StateNames.TryGetValue(Name,out value))
		{
			if(!CurrentStates.Contains(value) && !AddStates.Contains(value))
			{
				AddStates.Add(value);
				value.OnPushed();
			}
		}
		else
		{
			throw new UnityException("State Not Found");
		}
	}	
	public void Pop(string Name)
	{
		GUI_State value;
		if(StateNames.TryGetValue(Name,out value))
		{
			RemoveStates.Add(value);
			value.OnPopped();
		}
		else
		{
			throw new UnityException("State Not Found");
		}
	}
	
	//RPC Stuff for Lobby
	[RPC]
    public void SendMessages(string sender, string Msg)
	{
		if(!Msg.StartsWith("/"))
		{
			lobby.RecivedMessage(sender, Msg);	
		}
		else
		{
			string[] Command = Msg.Split(' ');
			if(Command[0].ToLower().StartsWith("/dm"))
			{
				if(Command[1] == UserName)
				{
					for(int i = 2; i < Command.Length; i++)
					{
						Msg += Command[i] + " ";
					}				
					
					lobby.RecivedMessage(sender, Msg);
				}
			}
			else if(Command[0].ToLower().StartsWith("/switch"))
			{
				lobby.SwitchTeam(Command[2],Command[1]);
			}
		}
		
	}
	
	[RPC]
    public void JoinedLobby(string sender)
	{
		lobby.RecivedMessage(sender, "Joined Lobby");
		lobby.JoinedLobby(sender);
	}	
	
	[RPC]
    public void LeaveLobby(string sender)
	{
		lobby.LeaveLobby(sender);
	}
	
	[RPC]
	public void ReadyToggle(string sender)
	{
		lobby.ToggleReady(sender);
	}
	
	[RPC]
	void StartGame()
	{
		Pop("Lobby");
		Push("spawn");
	}
}

