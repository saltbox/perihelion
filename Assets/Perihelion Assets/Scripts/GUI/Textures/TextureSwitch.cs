using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TextureSwitch : MonoBehaviour {
	
	public Texture[] textures;
	
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public Texture GetTexture(string name)
	{
		foreach (Texture tex in textures)
		{
			if (tex.name == name)
			{
				return tex;	
			}
		}	
		return null;
	}
		
		
}
