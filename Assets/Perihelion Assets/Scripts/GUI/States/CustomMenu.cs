using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CustomMenu : GUI_State {
	List<GameObject> mounts = new List<GameObject>();
	GameObject player;
	public GameObject[] prefabs;
	
	public CustomMenu(string Name, MainGUI_Manager manager, NetworkView net) : base(Name,manager,net)
	{
		prefabs = Camera.main.GetComponent<ModulePrefabs>().GetPrefabs();
	}
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public override void OnGUI ()
	{
		if (player == null)
		{
			player = GameObject.Find(Camera.main.GetComponent<MainGUI_Manager>().UserName);
			foreach (Transform hardpoint in player.transform)
			{
				mounts.Add(hardpoint.GetChild(0).gameObject);
			}
		}
		
		float x;
		float y;
		
		foreach (GameObject mount in mounts)
		{
			x = Camera.main.WorldToScreenPoint(mount.transform.position).x;
			y = Screen.height - Camera.main.WorldToScreenPoint(mount.transform.position).y;
			
			//weapon
			foreach(GameObject prefab in prefabs)
			{
				GameObject weapon = mount.GetComponent<GunMount>().module;
				//highlight which weapon is already mounted
				if (weapon != null && weapon.name.Contains(prefab.name))
				{
					GUI.color = Color.green;
				}
				
				//drawing the mount's button
				if(GUI.Button(new Rect(x, y, 90, 28), prefab.name))
				{
						mount.networkView.RPC("ChangeWeapon", RPCMode.All, prefab.name, Network.AllocateViewID());
				}
				GUI.color = Color.white;
				y += 30;
			}
			
			x += 92;
			y = Screen.height - Camera.main.WorldToScreenPoint(mount.transform.position).y;
			
			//weapon groups
			for (int i = 1; i <= 10; i++)
			{
				Hardpoint hp = mount.transform.parent.gameObject.GetComponent<Hardpoint>();
				//gridding the numbers
				if ((i - 1) % 3 == 0)
				{
					y = Screen.height - Camera.main.WorldToScreenPoint(mount.transform.position).y;
					x += 27;
				}
				//highlighting the current number
				if (hp.weaponGroups.Contains(i))
				{
					GUI.color = Color.green;
				}
				
				//drawing the number button
				if (GUI.Button(new Rect(x, y, 25, 24), i.ToString()))
				{
					if (hp.weaponGroups.Contains(i))
					{
						hp.weaponGroups.Remove(i);
					}
					else
					{
						hp.weaponGroups.Add(i);
					}
				}
				
				GUI.color = Color.white;
				y += 26;
				
			}
			
		}
	}
	
	
}
