using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class NameMenu : GUI_State {
	
	//Username
	public string CurrentUser {get; private set;}
	//List of Usernames Found
	List<string> NameList = new List<string>();
	
	//GUI State
	public bool Userfound;
	public bool AddNewUser;
	public bool DeleteUser;
	
	public NameMenu(string Name, MainGUI_Manager manager, NetworkView net) : base(Name,manager,net)
	{
		//Any Extra initialization for state
		GetUserFiles ();
		
		if(Userfound)
		{
			manager.Push("MainMenu");
			manager.Pop(this.Name);
		}
		
	}
	
	public override void Update ()
	{
		
	}
		
	
	public override void OnGUI()
	{
		int Y = 30;
		if(!Userfound)
		{
			addNewUser();
		}
		else
		{
			if(GUI.Button(new Rect(Screen.width - 205, Y, 200, 20), "Cancle") && !AddNewUser && !DeleteUser)
			{
				manager.Pop(this.Name);
			}
			Y += 30;
			if(GUI.Button(new Rect(Screen.width - 205, Y, 200, 20), "Add Name")&& !AddNewUser && !DeleteUser)
			{
				//Add User
				AddNewUser = true;
			}
			Y += 30;
			if(GUI.Button(new Rect(Screen.width - 205, Y, 200, 20), "Delete Name")&& !AddNewUser && !DeleteUser)
			{
				//Delete User
				DeleteUser = true;
			}
			Y += 30;
			
			if(NameList.Count > 1)
			{
				GUI.Label(new Rect(Screen.width - 205, Y, 200, 20),"				Select Name ");
				foreach(string name in NameList)
				{
					if(name != CurrentUser)
					{
						Y += 30;
						if(GUI.Button(new Rect(Screen.width - 205, Y, 200, 20), name)&& !AddNewUser && !DeleteUser)
						{
							WriteCurrentUser(name);
							manager.Pop(this.Name);
						}
					}
				}			
			}
			//Menu State
			if(AddNewUser)
			{
				addNewUser();
			}
			if(DeleteUser)
			{
				deleteUser();
			}
		}
	}
	
	
	
	string DeleteName = "";
	void deleteUser()
	{
		//Delete User Name
		GUI.BeginGroup(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 40, 200, 80));
		GUI.Box(new Rect(0, 0, 200, 100), "Enter A Name to Delete");
			
		DeleteName = GUI.TextField(new Rect(5, 25, 190, 20), DeleteName);
		if (GUI.Button(new Rect(5, 50, 75, 20), "Done"))
		{
			if(!NameList.Contains(DeleteName))
			{
				DeleteName = "Name Not Valid";
			}
			else if(CurrentUser == DeleteName)
			{
				DeleteName = "Name In Use";
			}
			else
			{
				//Write to files
				NameList.Remove(DeleteName);
				WriteToUsersFile(null);
				DeleteUser = false;
			}
		}
		if(Userfound)
		{
			if (GUI.Button(new Rect(120, 50, 75, 20), "Cancle"))
			{
				DeleteUser = false;
			}
		}
		
		GUI.EndGroup();
	}
	string NewUser = "";
	void addNewUser()
	{
		
		//Get New Name
		GUI.BeginGroup(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 40, 200, 80));
		GUI.Box(new Rect(0, 0, 200, 100), "Enter A New User Name");
			
		NewUser = GUI.TextField(new Rect(5, 25, 190, 20), NewUser);
		if (GUI.Button(new Rect(5, 50, 75, 20), "Done"))
		{
			if(NewUser.Contains(" ") || NewUser == "")
			{
				NewUser = "Name Not Valid";
			}
			else
			{
				//Write to files
				WriteToUsersFile(NewUser);
				WriteCurrentUser(NewUser);
				
				if(!Userfound)
				{
					manager.Push("MainMenu");
				}
				
				Userfound = true;
				AddNewUser = false;
				manager.Pop(this.Name);
				NewUser = "";
				
			}
		}
		if(Userfound)
		{
			if (GUI.Button(new Rect(120, 50, 75, 20), "Cancle"))
			{
				AddNewUser = false;
			}
		}
		
		GUI.EndGroup();
	}
	
	
	// User File Functions //
	void WriteToUsersFile (string Newname)
	{	
		StreamWriter writer = new StreamWriter("Users.Tryp");	
		
		if(Newname != null)
		{
			if(!NameList.Contains(Newname))
			{				
				NameList.Add(Newname);			
			}
		}
		foreach(string name in NameList)
		{
			writer.WriteLine(name);
			writer.Flush();
		}	
		writer.Close();
		
	}	
	void WriteCurrentUser(string name)
	{
		File.WriteAllText("User.Tryp",string.Empty);
		StreamWriter writer = new StreamWriter("User.Tryp");
		writer.WriteLine(name);
		CurrentUser = name;
		writer.Flush();
		writer.Close();
	}
	
	void GetUserFiles ()
	{
		//Check For Current User
		if(File.Exists("User.Tryp"))
		{
			StreamReader reader = new StreamReader("User.Tryp");		
			string line = "";
			
			if((line = reader.ReadLine()) != null)
			{
				CurrentUser = line;
				Userfound = true;
			}
			else
			{
				Userfound = false;
			}
			reader.Close();
		}
		else
		{
			File.Create("User.Tryp");
		}
		
		//Check For User Names file
		if(File.Exists("Users.Tryp"))
		{
			StreamReader reader = new StreamReader("Users.Tryp");			
			string line = "";
					
			while((line = reader.ReadLine()) != null)
			{
				NameList.Add(line);	
				if(!Userfound)
				{
					CurrentUser = line;
					Userfound = true;
				}
			}
			reader.Close();		
		}
		else
		{
			File.Create("Users.Tryp");
		}
	}
}
