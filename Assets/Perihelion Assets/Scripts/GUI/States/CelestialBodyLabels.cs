
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CelestialBodyLabels : GUI_State {
	
	public CelestialBodyLabels(string Name, MainGUI_Manager manager, NetworkView net) : base(Name,manager,net)
	{
		camera = Camera.mainCamera;
	}
	
	GameObject[] planets;
	GameObject[] moons;
	GameObject star;
	Camera camera;
	
	
	// Use this for initialization
	void Start () {
		
	}	
	
	Dictionary<GameObject, Color> color = new Dictionary<GameObject, Color>();
	Vector3 screenToWorld;
	Vector3 labelPosition;
	float temp;
	
	
	// Update is called once per frame
	public override void Update () {
		//while loops are used to account for errors that could arise if this is run before the solar system is created

			planets = GameObject.FindGameObjectsWithTag("Planet");
			moons = GameObject.FindGameObjectsWithTag("Moon");
			
			foreach(GameObject planet in planets)
			{
				if (!color.ContainsKey(planet))
				{
					color.Add(planet, new Color(255, 255, 255, 0));
				}
			}
			
			foreach(GameObject moon in moons)
			{
				if (!color.ContainsKey(moon))
				{
				color.Add(moon, new Color(255, 255, 255, 0));
				}
			}
			star = GameObject.FindGameObjectWithTag("Star");
	}
	
	Vector3 adjustedMousePosition;
	
	public override void OnGUI()
	{
		foreach (GameObject body in planets)
		{
			GUI.color = color[body];
			labelPosition = new Vector3(Camera.main.WorldToScreenPoint(body.transform.position).x, 
				Screen.height - Camera.main.WorldToScreenPoint(body.transform.position).y, 
				0);
			
			GUI.Label(new Rect(
				labelPosition.x, 
				labelPosition.y, 
				100,
				50),
				body.name);
			
			
			adjustedMousePosition = new Vector3(Input.mousePosition.x, Screen.height - (Input.mousePosition.y), 0);
	            
			if (Vector3.Distance(labelPosition, adjustedMousePosition) < 200 && color[body].a < 1)
			{				
				color[body] = new Color(1, 1, 1, color[body].a + 0.1f);	
			}
			else if (color[body].a > 0)
			{				
				color[body] = new Color(1, 1, 1, color[body].a - 0.1f);	
			}
			
		}	
		
		foreach (GameObject moon in moons)
		{
			GUI.color = color[moon];
			labelPosition = new Vector3(Camera.main.WorldToScreenPoint(moon.transform.position).x, 
				Screen.height - Camera.main.WorldToScreenPoint(moon.transform.position).y, 
				0);
			
			GUI.Label(new Rect(
				labelPosition.x, 
				labelPosition.y, 
				100,
				50),
				moon.name);
			
			
			adjustedMousePosition = new Vector3(Input.mousePosition.x, Screen.height - (Input.mousePosition.y), 0);
	            
			if (Vector3.Distance(labelPosition, adjustedMousePosition) < 50 && color[moon].a < 1)
			{				
				color[moon] = new Color(1, 1, 1, color[moon].a + 0.1f);	
			}
			else if (color[moon].a > 0)
			{				
				color[moon] = new Color(1, 1, 1, color[moon].a - 0.1f);	
			}
			
		}	
		GUI.color = Color.white;
		
	}
}
