using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IngameGUI : GUI_State {
	public List<GameObject> friends;
	public List<GameObject> targets;
	public GameObject localplayer;
	Camera playerCamera = GameObject.Find("Player Camera").camera;
	SmoothFollow follow;

	public IngameGUI(string Name, MainGUI_Manager manager, NetworkView net) : base(Name,manager,net)
	{
		playerCamera.pixelRect = new Rect(0, 0, 200, 200);
	}
	
	// Update is called once per frame
	public override void Update () {
		playerCamera.GetComponent<SmoothFollow>().target = Camera.main.GetComponent<SmoothFollowControls>().target;
	}
	
	
	float y;
	public override void OnGUI() 
	{
		y = 0;
		foreach (GameObject target in targets)
		{
			if (target.tag == "Moon")
				GUI.color = Color.grey;
			if (GUI.Button(new Rect(5, y, 130, 20), target.name))
			{
				localplayer.GetComponent<Player>().primaryTarget = target;
			}
			GUI.color = Color.white;
			y += 20;
		}
	
		GUI.Label(new Rect(5, 300, 200, 20), "Hull: " + localplayer.GetComponent<Player>().Hull);
		GUI.Label(new Rect(5, 325, 200, 20), "Shield: " + localplayer.GetComponent<Player>().Shield);
		
	}
}
