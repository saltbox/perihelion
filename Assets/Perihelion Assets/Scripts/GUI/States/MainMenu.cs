using UnityEngine;
using System.Collections;

public class MainMenu : GUI_State
{
	//Menu States
	bool showDirectConnect = false;
	bool Connecting = false;
	
	string ip = "142.177.28.12";
	public string UserName = "";
	
	public MainMenu(string Name, MainGUI_Manager manager, NetworkView net) : base(Name,manager,net)
	{
		//Any Extra initialization for state
	}
	
	public override void Update ()
	{
		UserName = manager.UserName;
	}
	
	void OnFailedToConnect(NetworkConnectionError error) {
        Debug.Log("Could not connect to server: " + error);
		Connecting = false;
    }
	
	public override void OnGUI ()
	{
		if(Connecting)
		{
			GUI.Label(new Rect(5, 5, 200, 20), "Connecting");
		}
		else
		{
			if(GUI.Button(new Rect(5, 5, 200, 20), "Direct Connect"))
			{
				showDirectConnect = true;
			}
		}
		
		if(Network.isClient)
		{
			this.manager.Push("Lobby");
			this.manager.Pop(this.Name);
		}
		
		if(GUI.Button(new Rect(Screen.width - 205, 5, 200, 20), UserName))
		{
			manager.Push("NameMenu");
		}
		
		
		if (GUI.Button(new Rect(5, 30, 200, 20), "Host"))
		{
			//HOST SERVER !!!
			Network.InitializeServer(32,5501,false);
			
			foreach(GameObject gameObject in Object.FindObjectsOfType(typeof(GameObject)))
			{
				gameObject.SendMessage("OnNetworkLoadedLevel",
					SendMessageOptions.DontRequireReceiver);
			}
			
			Debug.Log(Network.peerType);
			
			//CHANGE STATES
			this.manager.Push("Lobby");
			this.manager.Pop(this.Name);
		}
		
		if (showDirectConnect)
		{
			GUI.BeginGroup(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 40, 200, 80));
			GUI.Box(new Rect(0, 0, 200, 100), "Direct Connect");
			if (GUI.Button(new Rect(180, 5, 15, 15), ""))
			{
				showDirectConnect = false;	
			}
			ip = GUI.TextField(new Rect(5, 25, 190, 20), ip);
			
			if (GUI.Button(new Rect(5, 50, 75, 20), "Connect"))
			{
				//Connect!
				try
				{
					Network.Connect(ip,5501);
					Connecting = true;
				}
				catch
				{
					Connecting = false;
				}
				
				foreach(GameObject gameObject in Object.FindObjectsOfType(typeof(GameObject)))
				{
					gameObject.SendMessage("OnNetworkLoadedLevel",
						SendMessageOptions.DontRequireReceiver);
				}

				showDirectConnect = false;	
			}
			GUI.EndGroup();
		}
	}
}
