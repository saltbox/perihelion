using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Lobby : GUI_State {
	string message = "";
	string ChatText = "";
	public Dictionary<string,Team> teams;
	Dictionary<string,bool> tempPlayers; //used to save a dictionary of players in the GUI
	Team noTeam;
	string localPlayer;
	Vector2 scrollPosition;
	
	bool ready = false;
	bool oldReady = false;
	bool allReady = false;
	
	//Chat vars
	bool enterIsDown;
	bool enterPressed;
	
	public Lobby(string Name, MainGUI_Manager manager, NetworkView net) : base(Name,manager,net)
	{
		teams= new Dictionary<string, Team>();
		//Create Starting Teams
		NewTeam("Team 1");
		NewTeam("Team 2");
		NewTeam("Spectators");		
		ready = false;
	}
	
	public override void OnPushed ()
	{
		localPlayer = manager.UserName;		
		networkView.RPC("JoinedLobby",RPCMode.All,localPlayer);
	}
	
	public override void Update()
	{
	}	
	
	//Recive Message
	public void RecivedMessage(string Sender, string Msg)
	{
		ChatText += Sender + ">>" + Msg + "\n";
		scrollPosition.y += Mathf.Infinity;
	}
	
	//Join Lobby
	public void JoinedLobby(string Sender)
	{
		Team Spec;
		if(teams.TryGetValue("Spectators", out Spec))
		{
			//Join Spectators
			Spec.AddPlayer(Sender, false);
		}
	}
	
	//Toggle Ready
	public void ToggleReady(string Sender)
	{
		foreach (Team team in teams.Values)
		{
			bool val;
			if (team.GetPlayers().TryGetValue(Sender, out val))
			{
				team.GetPlayers()[Sender] = !val;
			}
				
		}		
	}
	
	public Team GetPlayerTeam(string playerName)
	{
		Team output = new Team("");
		foreach(Team team in teams.Values)
		{
			if (team.GetPlayers().ContainsKey(playerName))
			{
				output = team;
			}
		}
		return output;
	}
	
	//Switch Team
	public void SwitchTeam(string Sender, string TeamName)
	{
		Team NewTeam;
		if(teams.TryGetValue(TeamName, out NewTeam))
		{
			foreach (Team team in teams.Values)
			{
				if(team.GetPlayers().ContainsKey(Sender))
				{
					team.RemovePlayer(Sender);
				}
			}
			
			NewTeam.AddPlayer(Sender, ready);
		}
		else
		{
			//Team Doesn't Exist
		}
	}
	
	//Leave Lobby
	public void LeaveLobby(string Sender)
	{
		foreach (Team team in teams.Values)
		{
			if(team.GetPlayers().ContainsKey(Sender))
			{
				team.RemovePlayer(Sender);
			}
		}
	}
	
	void NewTeam(string arg)
	{
		string TeamName = "";
		foreach(char Char in arg)
		{
			if(Char != ' ')
			{
				TeamName += Char;
			}
			else
			{
				TeamName += '_';
			}	
		}
		
		if(!teams.ContainsKey(TeamName))
		{
			teams.Add(TeamName, new Team(TeamName));
		}
		else
		{
			//Team Already Exists
		}
	}
	
	int y;
	public override void OnGUI () {
		
		y = 0;
		
		//Message box
		GUI.SetNextControlName("TextField");
		message = GUI.TextField(new Rect(150, Screen.height - 40, Screen.width - 220, 20), message);
	 		
		//Send button
		GUI.SetNextControlName("SendButton");
		if ((GUI.Button(new Rect(Screen.width - 60, Screen.height - 45, 50, 30), "Send") || Event.current.keyCode == KeyCode.Return) && message != "")
		{			
			networkView.RPC("SendMessages",RPCMode.All,localPlayer, message);
			
			message = "";
			GUI.FocusControl("TextField");
		}
		
		//Leave button
		if (GUI.Button(new Rect(Screen.width - 63, Screen.height - 250, 60, 30), "Leave"))
		{
			networkView.RPC("LeaveLobby", RPCMode.All, localPlayer);
			Network.Disconnect();
			this.manager.Pop(this.Name);
			this.manager.Push("MainMenu");
		}
		
		//Begin button
		if (Network.isServer)
		{
			if (GUI.Button(new Rect(Screen.width - 63, Screen.height - 320, 60, 30), "Begin"))
			{
				if (allReady)
				{
					
					GameObject.Find("Net Object").GetComponent<CreateSolarSystem>().GenSystem(7);
					networkView.RPC("StartGame", RPCMode.All);
					
					//Camera.main.GetComponent<PublicVars>().mainGame = true;
				}
				else
				{
					message = "Host is attempting to start, but not all players are ready";
				}
			}
		}
		
		//Ready toggle
		ready = GUI.Toggle(new Rect(Screen.width - 63, Screen.height - 100, 60, 30), ready, "Ready");
		
		if (ready != oldReady)
		{
			networkView.RPC("ReadyToggle",RPCMode.All, localPlayer);
		}
		oldReady = ready;
		
		//Player list
		GUI.BeginGroup(new Rect(10, 10, 135, Screen.height - 10));
		foreach (Team team in teams.Values)
		{
			//Team
			if (GUI.Button(new Rect(0, y, 130, 20), team.name))
			{
				message = "/switch " + team.name + " " + localPlayer;
				GUI.FocusControl("TextField");				
			}
			
			y += 20;
			
			//Players
			tempPlayers = team.GetPlayers();
			foreach (string player in tempPlayers.Keys)
			{
				if (tempPlayers[player]) {
					GUI.color = Color.green;
					allReady = true;
				}
				else {
					GUI.color = Color.red;	
					allReady = false;
				}
				
				if (GUI.Button(new Rect(5, y, 130, 20), player))
				{					
					
					message = "/dm " + player;
					GUI.FocusControl("TextField");
				}
				
				GUI.color = Color.white;
				y += 20;				
			}
		}
		GUI.EndGroup();
		
		GUILayout.BeginArea(new Rect(150, 10, Screen.width - 200, Screen.height - 50));
    	scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Width (Screen.width - 220), GUILayout.Height (Screen.height - 50));
		GUI.skin.box.wordWrap = true;	
		GUI.skin.box.alignment =  TextAnchor.UpperLeft;
	    // We just add a single label to go inside the scroll view. Note how the
	    // scrollbars will work correctly with wordwrap.
	    GUILayout.Box(ChatText);
	
	    // End the scrollview we began above.
	    GUILayout.EndScrollView ();
	    GUILayout.EndArea();
		
	} //END GUI
}