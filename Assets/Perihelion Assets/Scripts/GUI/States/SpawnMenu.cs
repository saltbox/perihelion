using UnityEngine;
using System.Collections;

public class SpawnMenu : GUI_State {
	
	public SpawnMenu(string Name, MainGUI_Manager manager, NetworkView net) : base(Name,manager,net)
	{

	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	GameObject[] planets;
	
	public override void OnGUI () {
		planets = GameObject.FindGameObjectsWithTag("Planet");
		float y = 15;
		
		foreach (GameObject planet in planets)
		{
			if (GUI.Button(new Rect(5, y, 130, 20), planet.name))
			{
				Camera.main.GetComponent<SmoothFollowControls>().target = planet.transform;	
			}
			y += 20;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
