using UnityEngine;
using System.Collections;

public class EscMenu : GUI_State {
	
	public EscMenu(string Name, MainGUI_Manager manager, NetworkView net) : base(Name,manager,net)
	{
		
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public override void OnGUI() 
	{
		Debug.Log("esc menu");
		GUI.BeginGroup(new Rect((Screen.width / 2) - 100, (Screen.height / 2) - 150, 200, 300));
		GUI.color = Color.magenta;
		
		GUI.Box(new Rect(0, 0, 200, 300), "Menu");
		if (GUI.Button(new Rect(5, 20, 190,30), "Disconnect"))
		{
			Network.Disconnect();
			manager.Pop("esc");
			manager.Pop("inGame");
			manager.Push("MainMenu");
			foreach (GameObject obj in GameObject.FindObjectsOfType(typeof(GameObject)))
			{
				GameObject o = (GameObject)obj;
				if (o.name != "Main Camera" && o.name != "Net Object" && o.name != "Player Camera" && o.name != "Textures")
					GameObject.Destroy(o);
			}
		}
		GUI.color = Color.white;
		
		GUI.EndGroup();
	}
}
