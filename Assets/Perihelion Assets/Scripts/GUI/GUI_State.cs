using System;
using UnityEngine;

	public class GUI_State
	{
		public MainGUI_Manager manager;
		public string Name;
		public NetworkView networkView;
	
		public GUI_State (string Name, MainGUI_Manager manager,NetworkView networkView)
		{
			manager.StateNames.Add(Name,this);
			this.manager = manager;
			this.Name = Name;
			this.networkView = networkView;
		}
		
		public virtual void OnPushed()
		{
			
		}
	
		public virtual void OnPopped()
		{
		
		}
	
		public virtual void Update()
		{
		
		}
	
		public virtual void OnGUI()
		{
		
		}
	}

