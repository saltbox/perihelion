using UnityEngine;
using System.Collections;

public class Hardpoint : MonoBehaviour {
	
	public GameObject GunMount;
	public ArrayList weaponGroups;
	

	GunMount mountScript;
	public Player player;
	
	// Use this for initialization
	void Start () {
		weaponGroups = new ArrayList();
		GunMount = transform.GetChild(0).gameObject;
		GunMount.transform.rotation = transform.rotation;
		mountScript = GunMount.GetComponent<GunMount>();
		player = transform.parent.gameObject.GetComponent<Player>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void Fire()
	{
		transform.GetChild(0).gameObject.GetComponent<GunMount>().Fire();
	}
	
	public void Aim(Transform target)
	{
		transform.GetChild(0).gameObject.GetComponent<GunMount>().Aim(target);
	}
	
}
