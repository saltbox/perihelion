using UnityEngine;
using System.Collections;

public class GunMount : MonoBehaviour {
	
	public GameObject module;
	public Player player;
	bool canHit;
	GameObject[] prefabs;
	
	RaycastHit hit;
	Ray ray;
	
	// Use this for initialization
	void Start () {
		player = transform.parent.gameObject.GetComponent<Hardpoint>().player;
		prefabs = Camera.main.GetComponent<ModulePrefabs>().GetPrefabs();
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	[RPC]
	public void ChangeWeapon(string moduleName, int netID)		
	{
		transform.rotation = transform.parent.rotation;
		GameObject newModule;
		foreach (GameObject prefab in prefabs)
		{			
			newModule = prefab;
			if (prefab.name == moduleName)
			{
				if (transform.childCount > 0)
				{
					Destroy(transform.GetChild(0).gameObject);
				}
				
				newModule = (GameObject)Instantiate(newModule, Vector3.zero, transform.rotation);	
				newModule.networkView.viewID = netID;
				newModule.transform.parent = transform;
				newModule.transform.localPosition = new Vector3(0, 0, newModule.renderer.bounds.size.x / 2);
				newModule.transform.localRotation = transform.localRotation;
				
				//module = newModule;
				break;
			}
		}

		
	}
	
	public void Fire()
	{
		Weapon w = GetComponentInChildren<Weapon>();
		if (w != null)
			w.Fire();
	}
	
	public void Aim(Transform target)
	{
		Quaternion targetRotation = Quaternion.LookRotation(target.position - transform.position, Vector3.up);
		
		//Test for non-viable rotation angles
		Quaternion tempRotation = transform.rotation;
		transform.rotation = targetRotation;	
		
		if (transform.localRotation.eulerAngles.y > 90 && transform.localRotation.eulerAngles.y < 270)
		{
			targetRotation = transform.parent.rotation;
		}
		
		transform.rotation = tempRotation;
		targetRotation = Quaternion.RotateTowards(transform.rotation, targetRotation, Time.deltaTime * 100);
		transform.rotation = targetRotation;
	}
	
	public void ManualAim()
	{
		// Generate a plane that intersects the transform's position with an upwards normal.
    	Plane aimPlane = new Plane(Vector3.up, transform.position);
 
    	// Generate a ray from the cursor position
    	Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
 
    	// Determine the point where the cursor ray intersects the plane.
    	// This will be the point that the object must look towards to be looking at the mouse.
    	// Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
    	//   then find the point along that ray that meets that distance.  This will be the point
    	//   to look at.
    	float hitdist = 0.0f;
    	// If the ray is parallel to the plane, Raycast will return false.
    	if (aimPlane.Raycast (ray, out hitdist)) 
		{
        	// Get the point along the ray that hits the calculated distance.
        	Vector3 targetPoint = ray.GetPoint(hitdist);
 
        	// Determine the target rotation.  This is the rotation if the transform looks at the target point.
        	Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
 
        	// Smoothly rotate towards the target point.
        	transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 1 * Time.time);
		}
	}
}