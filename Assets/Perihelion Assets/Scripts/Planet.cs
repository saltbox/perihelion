using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Planet : MonoBehaviour {
	
	Dictionary<string, float> CaptureValues;
	
	// Use this for initialization
	void Start () 
	{	
		if(tag == "Planet")	
		{
			CaptureValues = new Dictionary<string, float>();
			
			foreach(string TeamName in Camera.mainCamera.GetComponent<MainGUI_Manager>().lobby.teams.Keys)
			{
				CaptureValues.Add(TeamName, 0f);
			}
		}
	}
	
	[RPC]
	public void Capture(string TeamName)
	{
		foreach(string key in CaptureValues.Keys)
		{
			float caprate = 0.1f;
			
			if(key == TeamName)
			{
				if(CaptureValues[key] < 99.9f)
				{
					CaptureValues[key] += caprate;
				}
				else
				{
					DustOrbit Comp = this.gameObject.GetComponent<DustOrbit>();
					Team team; 
					if(Camera.mainCamera.GetComponent<MainGUI_Manager>().lobby.teams.TryGetValue(key,out team))
					{
						Comp.teamColor = team.color;
						Comp.teamName = team.name;
					}			
				}
			}
			else
			{
				if(CaptureValues[key] > 0.1f)
				{
					CaptureValues[key] -= caprate;
				}
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnMouseOver() 
	{
		if (tag == "Planet" && Input.GetMouseButtonDown(0) && !isAlive(Camera.main.GetComponent<MainGUI_Manager>().UserName))
		{
			Camera.main.GetComponent<SmoothFollowControls>().SetTarget(transform);
			GameObject.Find("Net Object").GetComponent<PlayerSpawn>().SpawnPlayer(transform);
		}
	}
	
	bool isAlive(string playerName)
	{
		foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Player"))
		{
			if (obj.name == playerName)
			{
				return true;
			}
		}
		return false;
	}
}
