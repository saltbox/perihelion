using UnityEngine;
using System.Collections;

public class GravItem : MonoBehaviour {
	
	public Vector3 Velocity;
	
	GameObject Star;
	GameObject[] Planets;
	GameObject[] Moons;
	
	// Use this for initialization
	public virtual void Start () {	
		Planets = GameObject.FindGameObjectsWithTag("Planet");
		Moons = GameObject.FindGameObjectsWithTag("Moon");
		Star = GameObject.FindGameObjectWithTag("Star");
	}
	
	// Update is called once per frame
	public virtual void Update () {
		
		//Do Gravity		
		
		//Star
		Velocity += (0.1f * Vector3.Normalize(Star.transform.position - this.transform.position) *
            (this.transform.rigidbody.mass * Star.transform.rigidbody.mass) /
			(Vector3.Distance(this.transform.position, Star.transform.position)* 
			Vector3.Distance(this.transform.position, Star.transform.position)) / (this.transform.rigidbody.mass));
		
		//Planets
		foreach(GameObject Planet in Planets)
		{
			Velocity += (0.1f * Vector3.Normalize(Planet.transform.position - this.transform.position) *
           		(this.transform.rigidbody.mass * Planet.transform.rigidbody.mass) /
				(Vector3.Distance(this.transform.position, Planet.transform.position)* 
				Vector3.Distance(this.transform.position, Planet.transform.position)) / (this.transform.rigidbody.mass));
		}
		
		//Moons
		foreach(GameObject moon in Moons)
		{
			Velocity += (0.1f * Vector3.Normalize(moon.transform.position - this.transform.position) *
           		(this.transform.rigidbody.mass * moon.transform.rigidbody.mass) /
				(Vector3.Distance(this.transform.position, moon.transform.position)* 
				Vector3.Distance(this.transform.position, moon.transform.position)) / (this.transform.rigidbody.mass));
		}
		
	}
	
	public virtual void FixedUpdate ()
	{
	}
}
