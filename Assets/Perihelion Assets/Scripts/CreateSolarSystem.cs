using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Threading;

public class CreateSolarSystem : MonoBehaviour {
	
	
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public GameObject CelestialBody;
	public GameObject StarBody;		
	
	public void GenSystem(int planetCount) 
	{	
		//Generate a New System
		StarSystem NewSystem = new StarSystem(planetCount);
		
		int i = 300000;
		while(i >= 0)
		{
			NewSystem.tick();
			i --;
		}
		
		networkView.RPC("SpawnStar",RPCMode.All,
			NewSystem.StarName,
			NewSystem.StarMass);
		
		foreach(planet Planet in NewSystem.Planets)
		{
			//Spawn Planet
			networkView.RPC("SpawnCelestialBody",RPCMode.All,
				Planet.Name,
				"Planet",
				Planet.Mass,
				Planet.Pos,
				Planet.Velocity,
				new Vector3(150,150,150),
				NewSystem.StarName,
				Network.AllocateViewID());
			
			foreach(Moon moon in Planet.Moons)
			{
				//Spawn Moons
				networkView.RPC("SpawnCelestialBody",RPCMode.All,
					moon.Name,
					"Moon",
					moon.Mass,
					moon.Pos,
					moon.Velocity,
					new Vector3(20,20,20),
					Planet.Name,
					Network.AllocateViewID());
				
			}
		}
	}	
	
	[RPC]
	void SpawnStar(string Name, float Mass)
	{
		GameObject newBody = (GameObject) Instantiate(StarBody, new Vector3(0,0,0), Quaternion.identity);
			newBody.name = Name;
			newBody.rigidbody.mass = Mass;
			newBody.transform.localScale = new Vector3(160, 160, 160);
	}
	
	[RPC]
	void SpawnCelestialBody(string Name, string Tag, float Mass, Vector3 Pos, Vector3 Vel,  Vector3 Scale, string Parent, NetworkViewID NetID)
	{
		GameObject newBody = (GameObject) Instantiate(CelestialBody,Pos,Quaternion.identity);
			newBody.name = Name;
			newBody.tag = Tag;
			newBody.rigidbody.mass = Mass;
			newBody.GetComponent<DustOrbit>().ParentBody = GameObject.Find(Parent).transform;
			newBody.GetComponent<DustOrbit>().Velocity = Vel;
			newBody.transform.localScale = Scale;
			newBody.networkView.viewID = NetID;
	}
}

[System.Serializable]
public class StarSystem
{
	public string StarName;
	public float StarMass;
	
	public planet[] Planets;
	
	public StarSystem(int planetCount)
	{
		//Generate System
		StarName = "Star";
		StarMass = 3000;
		Planets = new planet[planetCount];
		
		for (int i = 0; i < planetCount; i++)
		{
			//Network.Instantiate(planet, transform.position, transform.rotation, 0);			
			planet NewPlanet = new planet(generateName((int)Mathf.Round(Random.value * 2) + 2),
				Mathf.Round(Random.value * StarMass / (3 * (i + 1) / 2)) + 250,
				new Vector3((Random.value * 5000) + 3000 * (i + 1),0,0));
			
			NewPlanet.Velocity.z = Mathf.Sqrt(((0.1f * (StarMass / NewPlanet.Mass ))* NewPlanet.Mass) /
				Vector3.Distance(Vector3.zero, NewPlanet.Pos ));
					
			for (int m = (int)Mathf.Round(Random.value * 2); m >= 0; m--)
			{
				
				Moon NewMoon = new Moon(generateName(2),
					Mathf.Round(Random.value * NewPlanet.Mass / 2) + 1,
					new Vector3(NewPlanet.Pos.x + (Random.value * 300 * m) + 200,0,0));
				
				NewMoon.Velocity.z = Mathf.Sqrt(((0.1f * (NewPlanet.Mass / NewMoon.Mass ))* NewMoon.Mass) /
					Vector3.Distance(NewPlanet.Pos, NewMoon.Pos ));
				
				NewPlanet.AddMoon(NewMoon);
			}
			Planets[i] = NewPlanet;
		}
	}
	
	public void tick()
	{	
		foreach(planet planet in Planets)
		{
			planet.Velocity += (0.1f * Vector3.Normalize(Vector3.zero - planet.Pos) *
            	(planet.Mass * StarMass) /
				(Vector3.Distance(planet.Pos, Vector3.zero)* 
				Vector3.Distance(planet.Pos, Vector3.zero)) / (planet.Mass));
			
			planet.Pos += planet.Velocity;
				
			foreach(Moon moon in planet.Moons)
			{
				moon.Velocity += (0.1f * Vector3.Normalize(planet.Pos - moon.Pos) *
            		(moon.Mass * planet.Mass) /
					(Vector3.Distance(moon.Pos, planet.Pos)* 
					Vector3.Distance(moon.Pos, planet.Pos)) / (moon.Mass));
			
				moon.Pos += moon.Velocity;
				moon.Pos += planet.Velocity;
			}
		}
	}
	
	string generateName(int length)
	{
		//LENGTH IS IN 2-LETTER SYLLABLES DO NOT FUCKING MISS THIS EVER
		string alpha = "bcdfghjklmnpqrstvwxyz";
		string alphaCaps = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		string vowel = "aeiou";
		string name = "";
		
		
		
		name += alphaCaps[(int)Mathf.Floor(Random.value * 21)];
		
		name += vowel[(int)Mathf.Floor(Random.value * 5)];
		
		for (int i = 0; i < length - 1; i++)
		{
			name += alpha[(int)Mathf.Floor(Random.value * 21)];	
			if (Random.value > 0.7)
			{
				name += alpha[(int)Mathf.Floor(Random.value * 21)];
			}
			name += vowel[(int)Mathf.Floor(Random.value * 5)];
		}
		
		if (Random.value > 0.6)
		{
			name += alpha[(int)Mathf.Floor(Random.value * 21)];
		}
		
		return name;
	}
}
[System.Serializable]
public class planet
{
	public string Name{get; private set;} 
	public float Mass{get; private set;} 
	public Vector3 Pos;
	public List<Moon> Moons;
	public Vector3 Velocity;
	
	public planet(string Name, float Mass, Vector3 Pos)
	{
		this.Mass = Mass;
		this.Name = Name;
		this.Pos = Pos;
		Moons = new List<Moon>();
	}
	
	public void AddMoon(Moon Value)
	{
		Moons.Add(Value);
	}
}
[System.Serializable]
public class Moon
{
	public string Name{get; private set;} 
	public float Mass{get; private set;} 
	public Vector3 Pos;
	public Vector3 Velocity;
	
	public Moon(string Name, float Mass, Vector3 Pos)
	{
		this.Mass = Mass;
		this.Name = Name;
		this.Pos = Pos;
	}
}
