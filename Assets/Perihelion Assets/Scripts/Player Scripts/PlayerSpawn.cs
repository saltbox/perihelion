using UnityEngine;
using System.Collections;

public class PlayerSpawn : MonoBehaviour {
	
	public GameObject PlayerCraft;
	
	
	
	// Use this for initialization
	void Start () {
	
	}
	
	
	
	public void SpawnPlayer(Transform Planet)
	{		
		// Instantiating SpaceCraft when Network is loaded		
		GameObject player = (GameObject)Network.Instantiate(PlayerCraft, Planet.position + new Vector3(750,0,0), transform.rotation,0);
		// Get Player Name
		player.name = Camera.main.GetComponent<MainGUI_Manager>().UserName;
		
		Camera.main.GetComponent<MainGUI_Manager>().Pop("spawn");
		Camera.main.GetComponent<MainGUI_Manager>().Push("inGame");
		Camera.main.GetComponent<SmoothFollowControls>().target = player.transform;
		
		
		player.GetComponent<Player>().Velocity.z += Mathf.Sqrt(((0.1f * (Planet.rigidbody.mass / player.rigidbody.mass ))* player.rigidbody.mass) /
				Vector3.Distance(Planet.position,player.transform.position));
	}

}
