using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : GravItem
{
	public float ShipAcceleration;
	public float ShipRotateSpeed;
	public float rotateVelocity;
	public GameObject Bulletfab;
	public GameObject Missilefab;
	public GameObject hardpointFab;
	public GameObject primaryTarget;
	public List<GameObject> potentialTargets = new List<GameObject>();
	public List<GameObject> allies = new List<GameObject>();
	public Color teamColor;
	public string teamName;
	
	public float Hull;
	public float Shield;
	public float ReservePower;
	public bool customMenu = false;
	public bool trackMouse = true;
	public bool escMenu = false;
	
	IngameGUI inGameGUI;
	MainGUI_Manager manager;
	
	// Use this for initialization
	public override void Start () {
		base.Start();
		
		manager = Camera.main.GetComponent<MainGUI_Manager>();
		
		teamColor = Color.blue;
		
		if(networkView.isMine)
		{
			networkView.RPC("SetName",RPCMode.All,Camera.main.GetComponent<MainGUI_Manager>().UserName);	
		}
		
		SetBaseVars();
		
		networkView.RPC("AddTarget", RPCMode.Others,  networkView.viewID);
		
		AddHardpoints();
		
		if (networkView.isMine)
		{
			inGameGUI.localplayer = gameObject;
		}
		
		foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Player"))
		{
			potentialTargets.Add(obj);
		}
		
		potentialTargets.Remove(gameObject);
		foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Moon"))
		{
			potentialTargets.Add(obj);	
		}
	}
	
	[RPC]
	public void SetName(string Name)
	{
		this.name = Name;
	}
	
	// Update is called once per frame
	public override void Update () {
		base.Update();

		if(networkView.isMine)
		{		
			SystemsCalculation();
			Controls ();
			Movement();
		}	
	}	
	
	void RotateToMouse()
	{
		// Generate a plane that intersects the transform's position with an upwards normal.
    	Plane playerPlane = new Plane(Vector3.up, transform.position);
 
    	// Generate a ray from the cursor position
    	Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
 
    	// Determine the point where the cursor ray intersects the plane.
    	// This will be the point that the object must look towards to be looking at the mouse.
    	// Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
    	//   then find the point along that ray that meets that distance.  This will be the point
    	//   to look at.
    	float hitdist = 0.0f;
    	// If the ray is parallel to the plane, Raycast will return false.
    	if (playerPlane.Raycast (ray, out hitdist)) 
		{
        	// Get the point along the ray that hits the calculated distance.
        	Vector3 targetPoint = ray.GetPoint(hitdist);
 
        	// Determine the target rotation.  This is the rotation if the transform looks at the target point.
        	Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
 
        	// Smoothly rotate towards the target point.
        	transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, ShipRotateSpeed * Time.time);
		}
	}
	
	void SystemsCalculation()
	{
		if (Shield < 100)
			Shield += 0.05f;
		else
			Shield = 100;
		if (potentialTargets != null)
			inGameGUI.targets = potentialTargets;
	}
	
	void Movement()
	{
        Velocity += transform.forward * (Input.GetAxis("Forward/Backward") * Time.deltaTime * ShipAcceleration);
    	Velocity += transform.right * (Input.GetAxis("Left/Right") * Time.deltaTime * ShipAcceleration / 2);
		
    	transform.position += Velocity * Time.deltaTime;
	}
	
	void Controls()
	{
		//Component Customization Menu
		if (Input.GetButtonDown("Customization Menu"))
		{
			if (customMenu)
			{
				manager.Pop("custom");
			}
			else
			{
				manager.Push("custom");
			}
			customMenu = !customMenu;
		}
		
		//Esc Menu
		if (Input.GetButtonDown("Esc Menu"))
		{
			if (escMenu)
				manager.Pop("esc");
			else
				manager.Push("esc");
			escMenu = !escMenu;
		}

		//Switching mouse tracking state
		if (Input.GetButtonDown("Toggle Mouse Aim") &&  !Input.GetButtonDown("Hold Mouse Aim"))
		{
			trackMouse = !trackMouse;
		}
		
		if (Input.GetButtonDown("Hold Mouse Aim"))
		{
			trackMouse = false;	
		}
		
		if (Input.GetButtonUp("Hold Mouse Aim"))
		{
			trackMouse = true;	
		}
	
		//Rotate to look at mouse
		if (trackMouse)
		{
			RotateToMouse();
		}
		
		//Aiming Weapons
		if (primaryTarget != null)
		{
			foreach (Transform hp in transform)
			{
				if (hp.gameObject.tag == "Hardpoint")
				{
					hp.gameObject.GetComponent<Hardpoint>().Aim(primaryTarget.transform);	
				}
			}
		}
		
		for (int i = 1; i <= 10; i++)
		{
			if (Input.GetButton("Weapon Group " + i))
			{
				Fire(i);
			}
		}
	}
	
	void Fire(int weaponGroup)
	{
		foreach(Hardpoint hardpoint in transform.GetComponentsInChildren<Hardpoint>())
		{
			if (hardpoint.weaponGroups.Contains(weaponGroup))
				hardpoint.Fire();
		}
	}
	
	void FireMissile()
	{
		GameObject missile = (GameObject) Network.Instantiate(Missilefab, transform.position + ((25 * transform.forward * (Random.value + 0.5f)) + ( 10 * transform.right * (Random.value - 0.5f) )),rigidbody.rotation,0);
		missile.GetComponent<Missile>().initialize(this.Velocity + (0.1f * transform.forward),primaryTarget);
	}
	
	void OnCollisionEnter(Collision collision) 
	{
		bool hit = false;
		if (collision.gameObject.tag == "Missile")
		{
			if (collision.gameObject.GetComponent<Missile>().lifeTime > 4)
			{
				hit = true;
			}
		}
		else
		{
			hit = true;	
		}
		
		if (hit)
		{
			Damage(10);
		}
	}
	
	public void Damage(float d)
	{
		Hull -= Mathf.Round(d / Shield);
		Shield -= d;
		if(Network.isServer && Hull <= 0)
		{
			networkView.RPC("RPC_Destroy",RPCMode.All);			
		}
	}
	
	[RPC]
	void RPC_Destroy()
	{
		Destroy(this.gameObject);		
		//Play Explostion Animation
		
		if(networkView.isMine)
		{
			Camera.main.GetComponent<SmoothFollowControls>().target = GameObject.FindGameObjectWithTag("Star").transform;
			Camera.main.GetComponent<MainGUI_Manager>().Pop("inGame");
			Camera.main.GetComponent<MainGUI_Manager>().Push("spawn");
		}
	}
	
	void OnGUI()
	{
		
	}
	
	void AddHardpoints()
	{
		//add turret hardpoints
		GameObject hardpoint;
		List<GameObject> hardPoints = new List<GameObject>();
		foreach (Transform child in transform)
		{
			if (child.tag == "Hardpoint Placeholder")
			{
				hardpoint = (GameObject)Instantiate(hardpointFab, child.position, child.rotation);
				hardpoint.transform.localPosition = child.localPosition;
				hardPoints.Add(hardpoint);				
			}
			GameObject.Destroy(child.gameObject);
		}
		
		Vector3 tempPosition;
		Quaternion tempRotation;
		foreach (GameObject hp in hardPoints)
		{
			tempPosition = hp.transform.localPosition;
			tempRotation = hp.transform.localRotation;			
			Debug.Log("temp position: " + tempPosition + " temp rotation: " + tempRotation.eulerAngles);
			hp.transform.parent = transform;
			hp.transform.localPosition = tempPosition;
			hp.transform.localRotation = tempRotation;
			hp.gameObject.tag = "Hardpoint";
		}
		//finished adding turret hardpoints
	}
	
	void SetBaseVars()
	{
		Lobby lobby = (Lobby)Camera.main.GetComponent<MainGUI_Manager>().getState("Lobby");
		Team playerTeam = lobby.GetPlayerTeam(gameObject.name);
		teamName = playerTeam.name;		
		teamColor = playerTeam.color;
		inGameGUI = (IngameGUI)Camera.main.GetComponent<MainGUI_Manager>().getState("inGame");
		Hull = 100;
		Shield = 100;
	}
	
	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) 
	{
		//Network Stuff
		if(stream.isWriting)
		{
			Vector3 Pos = transform.position;
			Quaternion Rotation = rigidbody.rotation;
			stream.Serialize(ref Velocity);
			stream.Serialize(ref Pos);
			stream.Serialize(ref Rotation);
		}
		else
		{
			Vector3 Pos = Vector3.zero;
			Quaternion Rotation = rigidbody.rotation;
			stream.Serialize(ref Velocity);
			stream.Serialize(ref Pos);
			stream.Serialize(ref Rotation);
			rigidbody.rotation = Rotation;
			transform.position = Pos;
		}
    }	
	
	[RPC]
	void AddTarget(NetworkViewID obj)
	{
		potentialTargets.Add(NetworkView.Find(obj).gameObject);
	}
	
}
