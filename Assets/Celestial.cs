using UnityEngine;
using System.Collections;

public class Celestial : MonoBehaviour {

    public float rotateVelocity;
    public Vector3 velocity;
	public float mass;
    public GameObject orbitingBody;
	public Texture[] texs;
    float maxSpeed;
	float parentMass;
	Light light;
	Material mat;
	
	Random rnd = new Random();
	
	void Start () {
		if (tag == "Star")
		{
			transform.position = Vector3.zero;
			transform.localScale = Vector3.one * 70;
			mass = 100;
			
			mat = new Material(Shader.Find("Self-Illumin/Diffuse"));
			mat.SetTexture("_MainTex", texs[0]);
			gameObject.renderer.material.color = Color.yellow;
			gameObject.renderer.material = mat;			
		}
		else if (tag == "Planet")
		{
			transform.localScale = Vector3.one * 14;
			transform.position = orbitingBody.transform.position + new Vector3(280, 0, 0);
			velocity = orbitingBody.GetComponent<Celestial>().velocity;
			parentMass = 80000;
			//parentMass = orbitingBody.GetComponent<Celestial>().mass;
			mass = 20;
			mat = new Material(Shader.Find("Diffuse"));
			mat.SetTexture("_MainTex", texs[0]);
			
			velocity.z += (float)Mathf.Sqrt((parentMass) / Vector3.Distance(transform.position, orbitingBody.transform.position));
		}
		else if (tag == "Moon")
		{
			transform.localScale = Vector3.one * 4;
			transform.position = orbitingBody.transform.position + new Vector3(50, 0, 0);
			velocity = orbitingBody.GetComponent<Celestial>().velocity;
			parentMass = 20;
			//parentMass = orbitingBody.GetComponent<Celestial>().mass;
			mass = 5.7f;
			mat = new Material(Shader.Find("Diffuse"));	
			mat.SetTexture("_MainTex", texs[0]);
			velocity.z += (float)Mathf.Sqrt((parentMass) / Vector3.Distance(transform.position, orbitingBody.transform.position));
		}
	}
	    
	// Update is called once per frame
	void Update () {
		if (tag != "Star")
		{
			velocity += (Vector3.Normalize(orbitingBody.transform.position - transform.position) * (mass * parentMass) / (Mathf.Pow(Vector3.Distance(transform.position, orbitingBody.transform.position), 2) / mass));
			transform.Translate(velocity * 0.001f, Space.World);
		}
		//shit's gotta rotate bro
        //transform.RotateAround(transform.up, rotateVelocity);
	}
}
